var request = require('supertest')
  , expect = require('expect.js')
  , app = require('./../src/static');

describe('static GET /', function(){
    it('should return "Hello world" for GET request', function(done){
        request(app)
            .get('/')
            .expect(function(res){
                expect(res.text).to.eql('Hello world');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});