var http = require('http')
  , request = require('supertest')
  , expect = require('expect.js')
  , Browser = require('zombie')
  , app = require('./../src/post-form');

describe('post-form GET /', function(){
    before(function() {
        this.server = http.createServer(app).listen(8888);
        // initialize the browser using the same port as the test application
        this.browser = new Browser({ site: 'http://localhost:8888' });
    });

    // load the page
    before(function(done) {
        this.browser.visit('/', done);
    });

    after(function(done){
        this.server.close(done);
    });

    it('should return form', function(done){
        this.browser.assert.success();
        this.browser.assert.element('form#form');
        this.browser.assert.attribute('form#form', 'method', 'post');
        this.browser.assert.element('input#name');
        done();
    })
});

describe('post-form POST /', function(){
    it('should return "Hello, josser" for POST request with name=josser', function(done){
        request(app)
            .post('/')
            .send('name=josser')
            .expect(function(res){
                expect(res.text).to.eql('Hello, josser');
                expect(res.status).to.eql(200);
            })
            .end(done);
    });

    it('should return "Hello, world" for POST request without name', function(done){
        request(app)
            .post('/')
            .expect(function(res){
                expect(res.text).to.eql('Hello, world');
                expect(res.status).to.eql(200);
            })
            .end(done);
    });

    it('should return "Hello, world" for POST request with empty name', function(done){
        request(app)
            .post('/')
            .send('name')
            .expect(function(res){
                expect(res.text).to.eql('Hello, world');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});