var request = require('supertest')
  , expect = require('expect.js')
  , async = require('async')
  , app = require('./../../../src/words')
  , wordsModel = require('./../../../src/words/server/models/words')
  , debug = require('./../../../src/words/server/lib/debug')(__filename);

describe('words GET /api/words', function(){
    beforeEach(function(done){
        wordsModel.flush();
        done();
    });

    it('should return empty list without words if there is no words', function(done){
        request(app)
            .get('/api/words')
            .expect(function(res){
                expect(res.body).to.be.an('array');
                expect(res.body).to.have.length(0);
                expect(res.status).to.eql(200);
            })
            .end(done);
    });

    it('should return list with one word if there is one word', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/words')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .get('/api/words')
                    .expect(function(res){
                        expect(res.body).to.be.an('array');
                        expect(res.body).to.have.length(1);
                        expect(res.body).to.eql([user]);
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('words GET /api/words/:id', function(){
    beforeEach(function(done){
        wordsModel.flush();
        done();
    });

    it('should return 404 if word with id does not exist', function(done){
        request(app)
            .get('/api/words/' + 1)
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.message).to.eql("Word not found");
                expect(res.body.error).to.eql("Not Found");
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return one word if there is one word', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/words')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .get('/api/words/' + user.id)
                    .expect(function(res){
                        expect(res.body).to.eql(user);
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('words POST /api/words', function(){
    beforeEach(function(done){
        wordsModel.flush();
        done();
    });

    it('should return error if required parameters are missing', function(done){
        request(app)
            .post('/api/words')
            .expect(function(res){
                expect(res.body.status).to.eql(400);
                expect(res.body.message).to.eql("Required parameters are missing");
                expect(res.body.error).to.eql("Bad Request");
                expect(res.status).to.eql(400);
            })
            .end(done);
    });

    it('should successfully create word if valid parameters were passed', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        request(app)
            .post('/api/words')
            .type('form')
            .send(user)
            .expect(function(res){
                expect(res.body.username).to.eql(user.username);
                expect(res.body.email).to.eql(user.email);
                expect(res.body).to.have.property('created');
                expect(res.body).to.have.property('id');

                user.created = res.body.created;
                user.id = res.body.id;

                expect(res.status).to.eql(201);
            })
            .end(done);
    });
});

describe('words PUT /api/words/:id', function(){
    beforeEach(function(done){
        wordsModel.flush();
        done();
    });

    it('should return error if word do not exist', function(done){
        request(app)
            .put('/api/words/' + 1)
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.message).to.eql("Word not found");
                expect(res.body.error).to.eql("Not Found");
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return error if required parameters were missing', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/words')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .put('/api/words/' + user.id)
                    .expect(function(res){
                        expect(res.body.status).to.eql(400);
                        expect(res.body.message).to.eql("Required parameters are missing");
                        expect(res.body.error).to.eql("Bad Request");
                        expect(res.status).to.eql(400);
                    })
                    .end(next);
            }
        ], done);
    });

    it('should return one updated word if there was one word updated after passing valid params', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/words')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                var newUserData = {
                    username: 'tset',
                    email: 'tset@ts.et'
                };
                request(app)
                    .put('/api/words/' + user.id)
                    .type('form')
                    .send(newUserData)
                    .expect(function(res){
                        expect(res.body.username).to.eql(newUserData.username);
                        expect(res.body.email).to.eql(newUserData.email);
                        expect(res.body.created).to.eql(user.created);
                        expect(res.body.id).to.eql(user.id);
                        expect(res.body.updated).to.be.greaterThan(user.created);

                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('words DELETE /api/words/:id', function(){
    beforeEach(function(done){
        wordsModel.flush();
        done();
    });

    it('should return error if word do not exist', function(done){
        request(app)
            .delete('/api/words/' + 1)
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.message).to.eql("Word not found");
                expect(res.body.error).to.eql("Not Found");
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should be able to delete word', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/words')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .delete('/api/words/' + user.id)
                    .expect(function(res){
                        expect(res.status).to.eql(204);
                    })
                    .end(next);
            },
            function(ponse, next){
                request(app)
                    .get('/api/words/' + user.id)
                    .expect(function(res){
                        expect(res.body.status).to.eql(404);
                        expect(res.body.message).to.eql("Word not found");
                        expect(res.body.error).to.eql("Not Found");
                        expect(res.status).to.eql(404);
                    })
                    .end(next);
            }
        ], done);
    });
});