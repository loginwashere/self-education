var request = require('supertest')
  , expect = require('expect.js')
  , async = require('async')
  , app = require('./../../../src/words')
  , usersModel = require('./../../../src/words/server/models/users')
  , debug = require('./../../../src/words/server/lib/debug')(__filename);

describe('words GET /api/users', function(){
    beforeEach(function(done){
        usersModel.flush()
            .then(function(result){
                done()
            })
            .catch(function(error){
                done(error);
            })
            .done();
    });

    it('should return empty list without users if there is no users', function(done){
        request(app)
            .get('/api/users')
            .expect(function(res){
                expect(res.body).to.be.an('array');
                expect(res.body).to.have.length(0);
                expect(res.status).to.eql(200);
            })
            .end(done);
    });

    it('should return list with one user if there is one user', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/users')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;
                        user.updated = null;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .get('/api/users')
                    .expect(function(res){
                        expect(res.body).to.be.an('array');
                        expect(res.body).to.have.length(1);
                        expect(res.body).to.eql([user]);
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('words GET /api/users/:id', function(){
    beforeEach(function(done){
        usersModel.flush()
            .then(function(result){
                done()
            })
            .catch(function(error){
                done(error);
            })
            .done();
    });

    it('should return 404 if user with id does not exist', function(done){
        request(app)
            .get('/api/users/' + 1)
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.message).to.eql("User not found");
                expect(res.body.error).to.eql("Not Found");
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return one user if there is one user', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/users')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;
                        user.updated = null;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .get('/api/users/' + user.id)
                    .expect(function(res){
                        expect(res.body).to.eql(user);
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('words POST /api/users', function(){
    beforeEach(function(done){
        usersModel.flush()
            .then(function(result){
                done()
            })
            .catch(function(error){
                done(error);
            })
            .done();
    });

    it('should return error if required parameters are missing', function(done){
        request(app)
            .post('/api/users')
            .expect(function(res){
                expect(res.body.status).to.eql(400);
                expect(res.body.message).to.eql("Required parameters are missing");
                expect(res.body.error).to.eql("Bad Request");
                expect(res.status).to.eql(400);
            })
            .end(done);
    });

    it('should successfully create user if valid parameters were passed', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        request(app)
            .post('/api/users')
            .type('form')
            .send(user)
            .expect(function(res){
                expect(res.body.username).to.eql(user.username);
                expect(res.body.email).to.eql(user.email);
                expect(res.body).to.have.property('created');
                expect(res.body).to.have.property('id');

                user.created = res.body.created;
                user.id = res.body.id;

                expect(res.status).to.eql(201);
            })
            .end(done);
    });
});

describe('words PUT /api/users/:id', function(){
    beforeEach(function(done){
        usersModel.flush()
            .then(function(result){
                done()
            })
            .catch(function(error){
                done(error);
            })
            .done();
    });

    it('should return error if user do not exist', function(done){
        request(app)
            .put('/api/users/' + 1)
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.message).to.eql("User not found");
                expect(res.body.error).to.eql("Not Found");
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return error if required parameters were missing', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/users')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .put('/api/users/' + user.id)
                    .expect(function(res){
                        expect(res.body.status).to.eql(400);
                        expect(res.body.message).to.eql("Required parameters are missing");
                        expect(res.body.error).to.eql("Bad Request");
                        expect(res.status).to.eql(400);
                    })
                    .end(next);
            }
        ], done);
    });

    it('should return one updated user if there was one user updated after passing valid params', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/users')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                var newUserData = {
                    username: 'tset',
                    email: 'tset@ts.et'
                };
                request(app)
                    .put('/api/users/' + user.id)
                    .type('form')
                    .send(newUserData)
                    .expect(function(res){
                        expect(res.body.username).to.eql(newUserData.username);
                        expect(res.body.email).to.eql(newUserData.email);
                        expect(res.body.created).to.eql(user.created);
                        expect(res.body.id).to.eql(user.id);
                        expect(res.body.updated).to.be.greaterThan(user.created);

                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('words DELETE /api/users/:id', function(){
    beforeEach(function(done){
        usersModel.flush()
            .then(function(result){
                done()
            })
            .catch(function(error){
                done(error);
            })
            .done();
    });

    it('should return error if user do not exist', function(done){
        request(app)
            .delete('/api/users/' + 1)
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.message).to.eql("User not found");
                expect(res.body.error).to.eql("Not Found");
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should be able to delete user', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(app)
                    .post('/api/users')
                    .type('form')
                    .send(user)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');

                        user.created = res.body.created;
                        user.id = res.body.id;

                        expect(res.status).to.eql(201);
                    })
                    .end(next);
            },
            function(response, next){
                request(app)
                    .delete('/api/users/' + user.id)
                    .expect(function(res){
                        expect(res.status).to.eql(204);
                    })
                    .end(next);
            },
            function(ponse, next){
                request(app)
                    .get('/api/users/' + user.id)
                    .expect(function(res){
                        expect(res.body.status).to.eql(404);
                        expect(res.body.message).to.eql("User not found");
                        expect(res.body.error).to.eql("Not Found");
                        expect(res.status).to.eql(404);
                    })
                    .end(next);
            }
        ], done);
    });
});