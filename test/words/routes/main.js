var http = require('http')
  , request = require('supertest')
  , expect = require('expect.js')
  , Browser = require('zombie')
  , app = require('./../../../src/words/');

describe('words GET /', function(){
    before(function() {
        this.server = http.createServer(app).listen(8888);
        // initialize the browser using the same port as the test application
        this.browser = new Browser({ site: 'http://localhost:8888' });
    });

    // load the page
    before(function(done) {
        this.browser.visit('/', done);
    });

    after(function(done){
        this.server.close(done);
    });

    it('should return form', function(done){
        this.browser.assert.success();
        this.browser.assert.element('h1', 'Wordscat');
        done();
    })
});