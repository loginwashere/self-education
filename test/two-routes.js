var request = require('supertest')
  , expect = require('expect.js')
  , app = require('./../src/two-routes');

describe('two-routes GET /hello', function(){
    it('should return "world" for GET request', function(done){
        request(app)
            .get('/hello')
            .expect(function(res){
                expect(res.text).to.eql('world');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});

describe('two-routes GET /foo', function(){
    it('should return "bar" for GET request', function(done){
        request(app)
            .get('/foo')
            .expect(function(res){
                expect(res.text).to.eql('bar');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});