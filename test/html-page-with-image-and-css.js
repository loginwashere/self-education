var http = require('http')
  , request = require('supertest')
  , expect = require('expect.js')
  , Browser = require('zombie')
  , app = require('./../src/html-page-with-image-and-css');

describe('html-page-with-image-and-css GET /', function(){
    before(function() {
        this.server = http.createServer(app).listen(8888);
        // initialize the browser using the same port as the test application
        this.browser = new Browser({ site: 'http://localhost:8888' });
    });

    // load the page
    before(function(done) {
        this.browser.visit('/', done);
    });

    after(function(done){
        this.server.close(done);
    });

    it('should return form', function(done){
        this.browser.assert.success();
        this.browser.assert.attribute('head link', 'href', "css/style.css");
        this.browser.assert.element('form#form');
        this.browser.assert.attribute('form#form', 'method', 'post');
        this.browser.assert.attribute('form#form', 'method', 'post');
        this.browser.assert.attribute('img', 'src', 'images/i-have-no-idea-what-im-doing.jpg');
        this.browser.assert.attribute('img', 'alt', "I have no idea what i'm doing");
        this.browser.assert.element('input#name');
        done();
    })
});
