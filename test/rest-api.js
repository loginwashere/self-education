var request = require('supertest')
  , expect = require('expect.js')
  , qs = require('querystring')
  , async = require('async')
  , pg = app.restApi.services.pg
  , api = require('./../src/rest-api');

describe('rest-api PATCH /', function(){
    it('should return not supported method error for PATCH request (or other unsupported method)', function(done){
        request(api)
            .patch('/')
            .expect(function(res){
                expect(res.body.status).to.eql(405);
                expect(res.body.error).to.eql('Bad request');
                expect(res.body.message).to.eql('Method Not Supported');
                expect(res.status).to.eql(405);
            })
            .end(done);
    });
});

describe('rest-api GET /', function(){
    beforeEach(function(done){
        pg.query("TRUNCATE TABLE users;", function(err, result){
            if (err) {
                done(err);
            }
            done();
        });
    });

    it('should return not found error for GET request to site root', function(done){
        request(api)
            .get('/')
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.error).to.eql('Bad request');
                expect(res.body.message).to.eql('Not found');
                expect(res.status).to.eql(404);
            })
            .end(done);
    });
});

describe('rest-api GET /api/users', function(){
    beforeEach(function(done){
        pg.query("TRUNCATE TABLE users;", function(err, result){
            if (err) {
                done(err);
            }
            done();
        });
    });

    it('should return empty array for GET request to /api/users without stored users', function(done){
        request(api)
            .get('/api/users')
            .expect(function(res){
                expect(res.body).to.be.an('array');
                expect(res.body).to.have.length(0);
                expect(res.status).to.eql(200);
            })
            .end(done);
    });

    it('should return created user for PUT request to /api/users', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(api)
                    .post('/api/users')
                    .send(qs.stringify(user))
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');
                        expect(res.status).to.eql(201);

                        user.id = res.body.id;
                    })
                    .end(next);
            },
            function(response, next){
                request(api)
                    .get('/api/users')
                    .expect(function(res){
                        expect(res.body).to.be.an('array');
                        expect(res.body).to.have.length(1);
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('rest-api GET /api/users/:id', function(){
    beforeEach(function(done){
        pg.query("TRUNCATE TABLE users;", function(err, result){
            if (err) {
                done(err);
            }
            done();
        });
    });

    it('should return not found error for GET request to /api/users without stored users', function(done){
        request(api)
            .get('/api/users/1')
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.error).to.eql('Bad request');
                expect(res.body.message).to.eql('Not found');
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return created user for PUT request to /api/users', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(api)
                    .post('/api/users')
                    .send(qs.stringify(user))
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');
                        expect(res.status).to.eql(201);

                        user.id = res.body.id;
                    })
                    .end(next);
            },
            function(response, next){
                request(api)
                    .get('/api/users/' + user.id)
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('rest-api POST /api/users', function(){
    beforeEach(function(done){
        pg.query("TRUNCATE TABLE users;", function(err, result){
            if (err) {
                done(err);
            }
            done();
        });
    });

    it('should return error for POST request without required params', function(done){
        request(api)
            .post('/api/users')
            .expect(function(res){
                expect(res.body.status).to.eql(400);
                expect(res.body.error).to.eql('Bad request');
                expect(res.body.message).to.eql('Missing parameters');
                expect(res.status).to.eql(400);
            })
            .end(done);
    });

    it('should return created user for POST request to /api/users', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        request(api)
            .post('/api/users')
            .send(qs.stringify(user))
            .expect(function(res){
                expect(res.body.username).to.eql(user.username);
                expect(res.body.email).to.eql(user.email);
                expect(res.body).to.have.property('created');
                expect(res.body).to.have.property('id');
                expect(res.status).to.eql(201);
            })
            .end(done);
    });
});

describe('rest-api PUT /api/users/:id', function(){
    beforeEach(function(done){
        pg.query("TRUNCATE TABLE users;", function(err, result){
            if (err) {
                done(err);
            }
            done();
        });
    });

    it('should return error for PUT request without required params', function(done){
        request(api)
            .put('/api/users')
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.error).to.eql('Bad request');
                expect(res.body.message).to.eql('Not found');
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return success status for PUT request to /api/users', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };
        async.waterfall([
            function(next){
                request(api)
                    .post('/api/users')
                    .send(qs.stringify(user))
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');
                        expect(res.status).to.eql(201);

                        user.id = res.body.id;
                    })
                    .end(next);
            },
            function(response, next){
                var updatedUser = {
                    username: 'test1',
                    email: 'test1@te.st'
                };
                request(api)
                    .put('/api/users/' + user.id)
                    .send(qs.stringify(updatedUser))
                    .expect(function(res){
                        expect(res.body).to.be.empty();
                        expect(res.status).to.eql(200);
                    })
                    .end(next);
            }
        ], done);
    });
});

describe('rest-api DELETE /api/users/:id', function(){
    beforeEach(function(done){
        pg.query("TRUNCATE TABLE users;", function(err, result){
            if (err) {
                done(err);
            }
            done();
        });
    });

    it('should return error for DELETE request without required params', function(done){
        request(api)
            .delete('/api/users')
            .expect(function(res){
                expect(res.body.status).to.eql(404);
                expect(res.body.error).to.eql('Bad request');
                expect(res.body.message).to.eql('Not found');
                expect(res.status).to.eql(404);
            })
            .end(done);
    });

    it('should return 204 for success DELETE request to /api/users', function(done){
        var user = {
            username: 'test',
            email: 'test@te.st'
        };

        async.waterfall([
            function(next){
                request(api)
                    .post('/api/users')
                    .send(qs.stringify(user))
                    .expect(function(res){
                        expect(res.body.username).to.eql(user.username);
                        expect(res.body.email).to.eql(user.email);
                        expect(res.body).to.have.property('created');
                        expect(res.body).to.have.property('id');
                        expect(res.status).to.eql(201);

                        user.id = res.body.id;
                    })
                    .end(next);
            },
            function(response, next){
                request(api)
                    .delete('/api/users/' + user.id)
                    .expect(function(res){
                        expect(res.body).to.be.empty();
                        expect(res.status).to.eql(204);
                    })
                    .end(next);
            }
        ], done);

    });
});