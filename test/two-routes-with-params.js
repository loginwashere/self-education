var request = require('supertest')
  , expect = require('expect.js')
  , app = require('./../src/two-routes-with-params');

describe('two-routes-with-params GET /', function(){
    it('should return "Hello world" for GET request', function(done){
        request(app)
            .get('/')
            .expect(function(res){
                expect(res.text).to.eql('Hello, world');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});

describe('two-routes-with-params GET /foo', function(){
    it('should return "bar" for GET request', function(done){
        request(app)
            .get('/foo')
            .expect(function(res){
                expect(res.text).to.eql('bar');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});

describe('two-routes-with-params GET /hello', function(){
    it('should return "world" for GET request', function(done){
        request(app)
            .get('/hello')
            .expect(function(res){
                expect(res.text).to.eql('Hello, world');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});

describe('two-routes-with-params GET /hello?name=josser', function(){
    it('should return "world" for GET request', function(done){
        request(app)
            .get('/hello?name=josser')
            .expect(function(res){
                expect(res.text).to.eql('Hello, josser');
                expect(res.status).to.eql(200);
            })
            .end(done);
    })
});