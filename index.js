
var http = require('http')
  , fs = require('fs')
  , taskName = process.argv[2] || process.env.TASK || 'static'
  , port = parseInt(process.argv[3], 10) || 8888;

var server = http.createServer(require('./src/' + taskName));

server.listen(port, function(){
    console.log("Server listening on: http://localhost:%s", port);
    console.log("Running task: %s", taskName);
    console.log("Available tasks: \n\t%s", fs.readdirSync('./src/').join("\n\t"));
});