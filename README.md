Self education project
======================

Environment
-----------

You can setup needed software yourself or use provided vagrant vm with provision script.

In ether case feel free to check it out to understand what's going on. (`bin/provision.sh`)

If you have vagrant installed run `vagrant up` to initiate vm creation.

After that connect to vm with `vagrant ssh` and navigate to project folder `/var/www/self-education`

To check out tasks run `node index`

By default `static` task will be launched

To start other tasks run `node index name`

Tasks
-----

where name is task name from available tasks list

[tasks list](https://docs.google.com/document/d/1GUAumezNGqrlbg5FSEq46PSvJzMEj1ufSB3JPtjxKV4/edit)

Linking with solutions:

1. `static`
2. `two-routes`
3. `two-routes-with-params`
4. `post-form`
5. `html-page-with-image-and-css`
6. `rest-api`
7. `words` - [express app](https://wordscat.herokuapp.com/)

To run `rest-api` task you need to provide `NODE_ENV` and have config for this env in `src/rest-api/config/`

To run `words` task execute command `node_modules/.bin/gulp` 

Tests
-----

To run tests use command `make` or `make test` in app dir

To check test coverage run `make test-cov`

To view test coverage run `make test-cov-view` and navigate to provided link

