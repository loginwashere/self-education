var qs = require('querystring')
  , jade = require('jade')
  , template = jade.compileFile(__dirname + '/views/index.jade');

function parseBody(request, next){
    var body = '';
    request.on('data', function (data) {
        body += data;

        // Too much POST data, kill the connection!
        if (body.length > 1e6){
            request.connection.destroy();
        }
    });
    request.on('end', function () {
        request.body = qs.parse(body);
        next(null, request);
    });
}

module.exports = function(request, response){
    switch (request.method) {
        case 'POST':
            parseBody(request, function(err, request){
                var name = request.body.name ? request.body.name : 'world';
                response.end("Hello, " + name);
            });
            break;
        case 'GET':
        default:
            response.end(template());
    }
};