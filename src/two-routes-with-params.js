var url = require('url')
  , qs = require('querystring');

module.exports = function(request, response){
    var responseString
      , parsedUrl = url.parse(request.url)
      , parsedParams = qs.parse(parsedUrl.query)
      , name = parsedParams.name ? parsedParams.name : 'world';

    switch (parsedUrl.pathname) {
        case '/foo':
            responseString = 'bar';
            break;
        case '/hello':
        default:
            responseString = 'Hello, ' + name;
    }

    response.end(responseString);
};