var url = require("url")
  , path = require("path")
  , fs = require("fs")
  , mime = require('mime');

module.exports = function(request, response){
    var uri = url.parse(request.url).pathname
      , filename = path.join(__dirname, 'public', uri);

    fs.exists(filename, function(exists) {
        if(!exists) {
            response.writeHead(404, {"Content-Type": "text/plain"});
            response.write("404 Not Found\n");
            response.end();
            return;
        }

        if (fs.statSync(filename).isDirectory()) {
            filename += '/index.html';
        }
        var stat = fs.statSync(filename);
        response.writeHead(200, {
            'Content-Type' : mime.lookup(filename),
            'Content-Length': stat.size
        });
        fs.createReadStream(filename).pipe(response);
    });
};