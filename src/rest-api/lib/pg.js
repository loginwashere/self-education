var pg = require('pg').native;

module.exports = function(config){
    var client = new pg.Client({
        user: config.user,
        password: config.password,
        database: config.database,
        host: config.host,
        port: config.port
    });
    client.connect();
    return client;
};