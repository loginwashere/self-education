var moment = require('moment')
  , pg = app.restApi.services.pg;

var users = module.exports = {};

users.create = function(data, next){
    var values = [
        data.username,
        data.email,
        moment(data.created).format('YYYY-MM-DDTHH:mm:ss.SSSZ')
    ];
    pg.query(
        {
            text: "INSERT INTO users (username, email, created) VALUES ($1, $2, $3)",
            values: values
        },
        function(err, result){
            if (err) {
                next(err);
            } else {
                next(null, result);
            }
        }
    )
};

users.update = function(id, data, next){
    var values = [
        data.username,
        data.email,
        moment(data.updated).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
        id
    ];
    pg.query(
        {
            text: "UPDATE users SET (username, email, updated) = ($1, $2, $3) WHERE id = $4",
            values: values
        },
        function(err, result){
            if (err) {
                next(err);
            } else {
                if (result && result.rowCount) {
                    next(null, result);
                } else {
                    next(null, null);
                }
            }
        }
    )
};

users.find = function(next){
    pg.query(
        "SELECT * FROM users",
        function(err, result){
            if (err) {
                next(err);
            } else {
                if (result  && result.rows) {
                    next(null, result.rows);
                } else {
                    next(null, null);
                }
            }
        }
    )
};

users.findOne = function(param, value, next){
    var values = [
        value
    ];
    pg.query(
        {
            text: "SELECT * FROM users WHERE " + param + " = $1",
            values: values
        },
        function(err, result){
            if (err) {
                next(err);
            } else {
                if (result  && result.rows && result.rows.length === 1) {
                    next(null, result.rows[0]);
                } else {
                    next(null, null);
                }
            }
        }
    )
};

users.delete = function(id, next){
    var values = [
        id
    ];
    pg.query(
        {
            text: "DELETE FROM users WHERE id = $1",
            values: values
        },
        function(err, result){
            if (err) {
                next(err);
            } else {
                if (result && result.rowCount) {
                    next(null, result);
                } else {
                    next(null, null);
                }
            }
        }
    )
};