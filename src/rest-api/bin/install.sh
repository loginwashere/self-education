#!/bin/bash

# Create/recreate pg databases and users
make rest-api-drop-db
make rest-api-init-db

# Up/reup migrations for vagrant env
NODE_ENV=vagrant make rest-api-migrate-down
NODE_ENV=vagrant make rest-api-migrate-up

# Up/reup migrations for test env
NODE_ENV=test make rest-api-migrate-down
NODE_ENV=test make rest-api-migrate-up
