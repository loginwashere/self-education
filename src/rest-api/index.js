require('./bootstrap');

var url = require('url')
  , qs = require('querystring')
  , routes = require('./routes/users');

function parseBody(request, next){
    var body = '';
    request.on('data', function (data) {
        body += data;

        // Too much POST data, kill the connection!
        if (body.length > 1e6){
            request.connection.destroy();
        }
    });
    request.on('end', function () {
        request.body = qs.parse(body);

        next(null, request);
    });
}

function sendResponse(error, response){
    if (error) {
        error.writeHead(error.status, {'Content-Type': 'application/json'});
        if (error.result) {
            error.write(JSON.stringify(error.result) + "\n");
        }
        error.end();
    } else {
        response.writeHead(response.status, {'Content-Type': 'application/json'});
        if (response.result) {
            response.write(JSON.stringify(response.result) + "\n");
        }
        response.end();
    }
}

module.exports = function(request, response){
    var pathname = url.parse(request.url).pathname;

    var foundRoutes = routes.filter(function(route){
        if ((!route.method || route.method === request.method) && route.regexp.test(pathname)) {
            var matches = pathname.match(route.regexp);

            route.params.forEach(function(param, paramsIndex){
                matches.forEach(function(match, index){
                    if (param.index === index) {
                        route.params[paramsIndex].value = match;
                    }
                });
            });
            return true;
        }
    });

    var route = foundRoutes[0];

    request.params = {};
    route.params.forEach(function(param){
        request.params[param.name] = param.value;
    });
    if (request.method === 'GET' || request.method === 'DELETE') {
        route.handler(request, response, function(error, response){
            if (error) {
                sendResponse(error)
            } else {
                sendResponse(null, response);
            }
        });
    } else if (request.method === 'POST' || request.method === 'PUT') {
        parseBody(request, function(err, request){
            if (err) {
                response.status = 500;
                response.result = {
                    status: 500,
                    error: 'Server Error',
                    message: 'Server Error',
                    err: err
                };
                sendResponse(response);
            } else {
                route.handler(request, response, function(error, response){
                    if (error) {
                        sendResponse(error)
                    } else {
                        sendResponse(null, response);
                    }
                });
            }
        });
    } else {
        response.status = 405;
        response.result = {
            status: 405,
            error: 'Bad request',
            message: 'Method Not Supported'
        };
        sendResponse(response);
    }
};