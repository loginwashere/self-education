require('./../bootstrap');

exports.up = function(next){
  app.restApi.services.pg.query(
      "CREATE SEQUENCE user_id_seq;"
    + "CREATE TABLE users ( "
      + "id smallint NOT NULL DEFAULT nextval('user_id_seq') primary key,"
      + "username varchar(20) NOT NULL,"
      + "email varchar(20) NOT NULL,"
      + "created timestamp default NULL,"
      + "updated timestamp default NULL"
    +");"
    + "ALTER SEQUENCE user_id_seq OWNED BY users.id;"
    +"ALTER TABLE users ADD CONSTRAINT unique_username UNIQUE (username);"
    +"ALTER TABLE users ADD CONSTRAINT unique_email UNIQUE (email);",
    function(err, result){
      if (err) {
        next(err);
      } else {
        next();
      }
    }
  );
};

exports.down = function(next){
  app.restApi.services.pg.query(
    "DROP TABLE IF EXISTS users ;",
    function(err, result){
      if (err) {
        next(err);
      } else {
        next();
      }
    }
  );
};
