CREATE USER api_user password 'password' ;
CREATE DATABASE self_education OWNER api_user ;

CREATE USER test_api_user password 'password' ;
CREATE DATABASE test_self_education OWNER test_api_user ;