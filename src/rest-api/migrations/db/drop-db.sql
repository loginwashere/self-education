
DROP DATABASE IF EXISTS self_education ;
DROP ROLE IF EXISTS api_user ;

DROP DATABASE IF EXISTS test_self_education ;
DROP ROLE IF EXISTS test_api_user ;