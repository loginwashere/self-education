
var config = require('./config')(process.env.NODE_ENV);

// app
var app = app || {};
global.app = global.app || {};
app.restApi =
    global.app.restApi = {
        services: {},
        config: config
    };

// pg
app.restApi.services.pg = require('./lib/pg')(config.pg);

/**
 * shutdown app
 */
app.restApi.shutdown = function() {
    app.restApi.services.pg.end();
};
