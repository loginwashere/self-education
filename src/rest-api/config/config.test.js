var config = module.exports = {};

config.pg = {};
config.pg.user = 'test_api_user';
config.pg.password = 'password';
config.pg.database = 'test_self_education';
config.pg.host = 'localhost';
config.pg.port = 5432;