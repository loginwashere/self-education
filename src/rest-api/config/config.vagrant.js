var config = module.exports = {};

config.pg = {};
config.pg.user = 'api_user';
config.pg.password = 'password';
config.pg.database = 'self_education';
config.pg.host = 'localhost';
config.pg.port = 5432;