var users = require('../models/users');

module.exports = [
    {
        regexp: /^\/api\/users\/([^/]+)$/,
        method: 'GET',
        params: [
            {
                index: 1,
                name: 'id',
                value: undefined
            }
        ],
        handler: function(request, response, next){
            users.findOne('id', request.params.id, function(err, result){
                if (err) {
                    response.status = 500;
                    response.result = {
                        status: 500,
                        error: 'Server Error',
                        message: 'Server Error',
                        err: err
                    };
                    next(response);
                } else {
                    if (result) {
                        response.status = 200;
                        response.result = result;
                    } else {
                        response.status = 404;
                        response.result = {
                            status: 404,
                            error: 'Bad request',
                            message: 'Not found'
                        };
                    }
                    next(null, response);
                }
            });
        }
    },
    {
        regexp: /^\/api\/users$/,
        method: 'GET',
        params: [],
        handler: function(request, response, next){
            users.find(function(err, result){
                if (err) {
                    response.status = 500;
                    response.result = {
                        status: 500,
                        error: 'Server Error',
                        message: 'Server Error',
                        err: err
                    };
                    next(response);
                } else {
                    response.status = 200;
                    response.result = result;
                    next(null, response);
                }
            });
        }
    },
    {
        regexp: /^\/api\/users$/,
        method: 'POST',
        params: [
            {
                index: 1,
                name: 'id',
                value: undefined
            }
        ],
        handler: function(request, response, next){
            if (!request.body.username || !request.body.email) {
                response.status = 400;
                response.result = {
                    status: 400,
                    error: 'Bad request',
                    message: 'Missing parameters'
                };
                next(response);
            }
            var user = {};

            user.username = request.body.username;
            user.email = request.body.email;
            user.created = Date.now();

            users.create(user, function(err, result){
                if (err) {
                    response.status = 500;
                    response.result = {
                        status: 500,
                        error: 'Server Error',
                        message: 'Server Error',
                        err: err
                    };
                    next(response);
                } else {
                    users.findOne('username', user.username, function(err, result){
                        if (err) {
                            response.status = 500;
                            response.result = {
                                status: 500,
                                error: 'Server Error',
                                message: 'Server Error',
                                err: err
                            };
                            next(response);
                        } else {
                            response.status = 201;
                            response.result = result;

                            next(null, response);
                        }
                    });
                }
            });
        }
    },
    {
        regexp: /^\/api\/users\/([^/]+)$/,
        method: 'PUT',
        params: [
            {
                index: 1,
                name: 'id',
                value: undefined
            }
        ],
        handler: function(request, response, next){
            var user = {};
            if (request.body.username || request.body.email) {
                if (request.body.username) {
                    user.username = request.body.username;
                }
                if (request.body.email) {
                    user.email = request.body.email;
                }
                user.updated = Date.now();
            }
            users.update(request.params.id, user, function(err, result){
                if (err) {
                    response.status = 500;
                    response.result = {
                        status: 500,
                        error: 'Server Error',
                        message: 'Server Error',
                        err: err
                    };
                    next(response);
                } else {
                    if (result) {
                        response.status = 200;
                        next(null, response);
                    } else {
                        response.status = 404;
                        response.result = {
                            status: 404,
                            error: 'Bad request',
                            message: 'Not found'
                        };
                        next(response);
                    }
                }
            });
        }
    },
    {
        regexp: /^\/api\/users\/([^/]+)$/,
        method: 'DELETE',
        params: [
            {
                index: 1,
                name: 'id',
                value: undefined
            }
        ],
        handler: function(request, response, next){
            users.delete(request.params.id, function(err, result){
                if (err) {
                    response.status = 500;
                    response.result = {
                        status: 500,
                        error: 'Server Error',
                        message: 'Server Error',
                        err: err
                    };
                    next(response);
                } else {
                    if (result) {
                        response.status = 204;
                        next(null, response);
                    } else {
                        response.status = 404;
                        response.result = {
                            status: 404,
                            error: 'Bad request',
                            message: 'Not found'
                        };
                        next(response);
                    }
                }
            });
        }
    },
    {
        regexp: /^.*$/,
        params: [],
        handler: function(request, response, next){
            response.status = 404;
            response.result = {
                status: 404,
                error: 'Bad request',
                message: 'Not found'
            };
            next(response);
        }
    }
];