var url = require('url');

module.exports = function(request, response){
    var responseString
      , pathname = url.parse(request.url).pathname;

    switch (pathname) {
        case '/hello':
            responseString = 'world';
            break;
        case '/foo':
            responseString = 'bar';
            break;
        default:
            responseString = 'Hello world';
    }

    response.end(responseString);
};