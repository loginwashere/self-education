function isArray(value){
    return !(value.constructor.toString().indexOf("Array") === -1);
}

function getWords(callback, errorCallback){
    var searchUrl = 'https://wordscat.herokuapp.com/api/users';
    var x = new XMLHttpRequest();
    x.open('GET', searchUrl);
    x.responseType = 'json';
    x.onload = function(){
        var response = x.response;

        if (!response || !response.length === 0) {
            errorCallback('No response from Google Image search!');
            return;
        }

        console.assert(isArray(response), 'Unexpected respose from the Google Image Search API!');

        callback(response);
    };
    x.onerror = function(){
        errorCallback('Network error.');
    };
    x.send();
}

function renderStatus(statusText){
    document.getElementById('status').textContent = statusText;
}

document.addEventListener('DOMContentLoaded', function(){
    getWords(function(words){

        renderStatus('Service has: ' + words.length + ' words');

        var wordsContainer = document.getElementById('words');

        var html = [];

        html.push('<ul class="words">');
        words.forEach(function(word){
            html.push('<li class="item">' + word.username + '</li>');
        });

        html.push('</ul>');

        wordsContainer.innerHTML = html.join('');
    }, function(errorMessage){
        renderStatus('Cannot display image. ' + errorMessage);
    });
});
