function preparePostData(data){
    var query = [];
    for (var key in data) {
        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
    }
    return query.join('&');
}

// The onClicked callback function.
function onClickHandler(info, tab){
    console.log("item " + info.menuItemId + " was clicked");
    console.log("info: " + JSON.stringify(info));
    console.log("tab: " + JSON.stringify(tab));

    var data = {
        username: info.selectionText,
        email: info.selectionText + '@te.st'
    };

    var url = 'https://wordscat.herokuapp.com/api/users';

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function(){
        // do something to response
        console.log(this.responseText);
    };
    xhr.send(preparePostData(data));
}

chrome.contextMenus.onClicked.addListener(onClickHandler);

// Set up context menu tree at install time.
chrome.runtime.onInstalled.addListener(function(){
    var context = "selection";
    var title = "Add word to WordsCat";
    var id = chrome.contextMenus.create({
        "title": title,
        "contexts": [context],
        "id": "context" + context
    });
    console.log("'" + context + "' item:" + id);

    // Intentionally create an invalid item, to show off error checking in the
    // create callback.
    console.log("About to try creating an invalid item - an error about " +
    "duplicate item child1 should show up");
    chrome.contextMenus.create({"title": "Oops", "id": "child1"}, function(){
        if (chrome.extension.lastError) {
            console.log("Got expected error: " + chrome.extension.lastError.message);
        }
    });
});