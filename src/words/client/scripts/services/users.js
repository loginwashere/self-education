'use strict';

exports.inject = function(app) {
    app.factory('UsersService', ['$http', exports.factory]);
    return exports.factory;
};

exports.factory = function($http) {
    return {
        getAll: function(){
            return $http({
                method: 'GET',
                url: '/api/users'
            })
                .then(function(data){
                    return data.data;
                });
        },
        getOne: function(id){
            return $http({
                method: 'GET',
                url: '/api/users/' + id
            })
                .then(function(data){
                    return data.data;
                });
        },
        delete: function(id){
            return $http({
                method: 'DELETE',
                url: '/api/users/' + id
            });
        },
        create: function(data){
            var req = {
                method: 'POST',
                url: '/api/users',
                headers: {'Content-Type': "application/x-www-form-urlencoded"},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: data
            };
            return $http(req);
        },
        update: function(id, data){
            var req = {
                method: 'PUT',
                url: '/api/users/' + id,
                headers: {'Content-Type': "application/x-www-form-urlencoded"},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: data
            };
            return $http(req);
        }
    };
};