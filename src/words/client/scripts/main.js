'use strict';

require('angular');

require('angular-bootstrap');
var uiRouter = require('angular-ui-router');

var app = angular.module('WordsCat', [
    'ui.bootstrap',
    uiRouter
]);

require('./services/users').inject(app);

require('./controllers/users/new.modal').inject(app);
require('./controllers/users/edit.modal').inject(app);

app.config(function($locationProvider, $stateProvider, $urlRouterProvider) {
    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise('/404');

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'views/home.html',
            controller: require('./controllers/home').inject(app)
        })
        .state('users', {
            abstract: true,
            url: '/users',
            templateUrl: 'views/users/users.html',
            controller: require('./controllers/users/list').inject(app)
        })
        .state('users.new', {
            url: '/new',
            templateUrl: 'views/users/users.new.html',
            controller: require('./controllers/users/new').inject(app)
        })
        .state('users.edit', {
            url: '/{id:[0-9]{1,4}}/edit',
            templateUrl: 'views/users/users.edit.html',
            controller: require('./controllers/users/edit').inject(app)
        })
        .state('users.list', {
            url: '',
            templateUrl: 'views/users/users.list.html',
            controller: require('./controllers/users/list').inject(app)
        })
        .state('users.view', {
            url: '/{id:[0-9]{1,4}}/view',
            templateUrl: 'views/users/users.view.html',
            controller: require('./controllers/users/view').inject(app)
        })
        .state('words', {
            url: '/words',
            templateUrl: 'views/words/list.html',
            controller: require('./controllers/words/list').inject(app)
        })
        .state('404', {
            url: "/404",
            templateUrl: 'views/404.html'
        });
});

app.run();