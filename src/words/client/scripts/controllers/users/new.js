'use strict';

exports.inject = function(app) {
    app.controller('UserNewCtrl', [
        '$scope',
        '$stateParams',
        '$state',
        'UsersService',
        exports.controller
    ]);
    return exports.controller;
};

exports.controller = function($scope, $stateParams, $state, UsersService) {
    $scope.user = {};

    $scope.create = function(){
        UsersService.create($scope.user)
            .then(function(user){
                $state.go('users.list');
            });
    };
};