'use strict';

exports.inject = function(app) {
    app.controller('UserNewModalCtrl', [
        '$scope',
        '$modalInstance',
        'UsersService',
        '$log',
        exports.controller
    ]);
    return exports.controller;
};

exports.controller = function($scope, $modalInstance, UsersService, $log) {
    $scope.user = {};

    $scope.ok = function () {
        UsersService.create($scope.user)
            .then(function(user){
                $log.info('UserNewModalCtrl ok user', user);
                $modalInstance.close($scope.user);
            });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};