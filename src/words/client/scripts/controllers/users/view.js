'use strict';

exports.inject = function(app) {
    app.controller('UserViewCtrl', [
        '$scope',
        '$stateParams',
        'UsersService',
        exports.controller
    ]);
    return exports.controller;
};

exports.controller = function($scope, $stateParams, UsersService) {
    UsersService.getOne($stateParams.id)
        .then(function(user){
            $scope.user = user;
        });
};