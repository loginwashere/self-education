'use strict';

exports.inject = function(app) {
    app.controller('UserEditModalCtrl', [
        '$scope',
        '$stateParams',
        '$modalInstance',
        'UsersService',
        'id',
        '$log',
        exports.controller
    ]);
    return exports.controller;
};

exports.controller = function($scope, $stateParams, $modalInstance, UsersService, id, $log) {
    $log.info('UserEditModalCtrl arguments', arguments);
    $scope.user = {};

    UsersService.getOne(id)
        .then(function(user){
            $scope.user = user;
        });

    $scope.ok = function () {
        UsersService.update($scope.user.id ,$scope.user)
            .then(function(user){
                $log.info('UserNewModalCtrl ok user', user);
                $modalInstance.close($scope.user);
            });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};