'use strict';

exports.inject = function(app) {
    app.controller('UserEditCtrl', [
        '$scope',
        '$stateParams',
        '$state',
        'UsersService',
        exports.controller
    ]);
    return exports.controller;
};

exports.controller = function($scope, $stateParams, $state, UsersService) {
    $scope.user = {};

    UsersService.getOne($stateParams.id)
        .then(function(user){
            $scope.user = user;
        });

    $scope.update = function(){
        UsersService.update($scope.user.id ,$scope.user)
            .then(function(user){
                $state.go('users.list');
            });
    };
};