'use strict';



exports.inject = function(app) {
    app.controller('UsersListCtrl', [
        '$scope',
        '$state',
        '$modal',
        'UsersService',
        '$log',
        exports.controller
    ]);
    return exports.controller;
};

exports.controller = function($scope, $state, $modal, UsersService, $log) {
    function getAll(){
        UsersService.getAll()
            .then(function(users){
                $scope.users = users;
            });
    }
    getAll();

    $scope.delete = function(id){
        UsersService.delete(id)
            .then(function(users){
                getAll();
                $state.go('users.list');
            });
    };

    $scope.createModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'views/users/users.new.modal.html',
            controller: 'UserNewModalCtrl',
            size: 'lg'
        });

        modalInstance.result.then(function(user){
            $log.info('UsersListCtrl createModal result user', user);
            getAll();
            $state.go('users.list');
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.editModal = function (params) {
        var modalInstance = $modal.open({
            templateUrl: 'views/users/users.edit.modal.html',
            controller: 'UserEditModalCtrl',
            size: 'lg',
            resolve: {
                id: function(){
                    return params.id
                }
            }
        });

        modalInstance.result.then(function(user){
            $log.info('UsersListCtrl editModal result user', user);
            getAll();
            $state.go('users.list');
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
};