
var express = require('express')
  , app = express()
  , logger = require('morgan')
  , routes = require('./server/routes')
  , bodyParser = require('body-parser');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.set('views', __dirname + '/server/views');
app.set('view engine', 'jade');

routes(app);

module.exports = app;