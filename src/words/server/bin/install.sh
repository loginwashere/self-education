#!/bin/bash

# Create/recreate pg databases and users
make words-drop-db
make words-init-db

# Up/reup migrations for vagrant env
NODE_ENV=vagrant make words-migrate-down
NODE_ENV=vagrant make words-migrate-up

# Up/reup migrations for test env
NODE_ENV=test make words-migrate-down
NODE_ENV=test make words-migrate-up
