var express = require('express')
  , router = express.Router()
  , wordsModel = require('./../models/words')
  , debug = require('./../lib/debug')(__filename);

router.param('id', function(req, res, next, id) {
    id = parseInt(id, 10);
    req.word = wordsModel.getOne(id);
    if (!req.word) {
        res.status(404).send({
            status: 404,
            message: "Word not found",
            error: "Not Found"
        });
    }
    next();
});

router.route('/words/:id')
    .get(function(req, res, next) {
        res.json(req.word);
    })
    .put(function(req, res, next) {
        if (!req.body.username && !req.body.email) {
            res.status(400).send({
                status: 400,
                message: "Required parameters are missing",
                error: "Bad Request"
            });
        }
        res.json(wordsModel.update(req.word.id, req.body));
    })
    .delete(function(req, res, next) {
        wordsModel.delete(req.word.id);
        res.status(204).end();
    });

router.route('/words')
    .get(function(req, res, next) {
        res.json(wordsModel.getAll());
    })
    .post(function(req, res, next) {
        if (!req.body.username || !req.body.email) {
            res.status(400).send({
                status: 400,
                message: "Required parameters are missing",
                error: "Bad Request"
            });
        }
        res.status(201).json(wordsModel.create(req.body));
    });

module.exports = router;