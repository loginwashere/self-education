var path = require('path')
  , express = require('express')
  , favicon = require('serve-favicon')
  , userRoutes = require('./users')
  , wordsRoutes = require('./words');

module.exports = function(app){
    app.use('/api', userRoutes);
    app.use('/api', wordsRoutes);

    app.use(favicon(__dirname + '/../../public/favicon.ico'));

    app.use(express.static(__dirname + '/../../public'));
    app.use(express.static(__dirname + '/../../../../bower_components'));

    app.all('/*', function(req, res, next){
        if (req.path.indexOf('/api/') !== -1) {
            return res.status(404).end();
        }
        // Just send the index.html for other files to support HTML5Mode
        res.sendFile('index.html', {root: path.join(__dirname, '..', '..', 'public')});
    });
};