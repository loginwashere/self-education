var express = require('express')
  , router = express.Router()
  , usersModel = require('./../models/users')
  , debug = require('./../lib/debug')(__filename);

router.param('id', function(req, res, next, id) {
    id = parseInt(id, 10);
    usersModel.findOne('id', id)
        .then(function(user){
            req.user = user;
            if (!req.user) {
                res.status(404).send({
                    status: 404,
                    message: "User not found",
                    error: "Not Found"
                });
            }
            next();
        })
        .catch(function(error){
            next(error);
        })
        .done();
});

router.route('/users/:id')
    .get(function(req, res, next) {
        res.json(req.user);
    })
    .put(function(req, res, next) {
        if (!req.body.username && !req.body.email) {
            return res.status(400).send({
                status: 400,
                message: "Required parameters are missing",
                error: "Bad Request"
            });
        }
        usersModel.update(req.user.id, req.body)
            .then(function(result){
                return usersModel.findOne('id', req.user.id)
                    .then(function(result){
                        res.status(200).json(result);
                    });
            })
            .catch(function(error){
                next(error);
            })
            .done();
    })
    .delete(function(req, res, next) {
        usersModel.delete(req.user.id)
            .then(function(result){
                res.status(204).end();
            })
            .catch(function(error){
                next(error);
            })
            .done();
    });

router.route('/users')
    .get(function(req, res, next) {
        usersModel.find()
            .then(function(result){
                res.json(result);
            })
            .catch(function(error){
                next(error);
            })
            .done();
    })
    .post(function(req, res, next) {
        if (!req.body.username || !req.body.email) {
            res.status(400).send({
                status: 400,
                message: "Required parameters are missing",
                error: "Bad Request"
            });
        }
        usersModel.create(req.body)
            .then(function(result){
                return usersModel.findOne('username', req.body.username)
                    .then(function(result){
                        res.status(201).json(result);
                    });
            })
            .catch(function(error){
                next(error);
            })
            .done();
    });

module.exports = router;