var moment = require('moment')
  , Q = require('q')
  , debug = require('./../lib/debug')(__filename)
  , pg = require('../lib/pg');

var users = module.exports = {};

users.create = function(data){
    var values = [
        data.username,
        data.email,
        moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ')
    ];
    return pg()
        .then(function(client){
            return Q.ninvoke(client, "query", {
                    text: "INSERT INTO users (username, email, created) VALUES ($1, $2, $3)",
                    values: values
                });
        });
};

users.update = function(id, data){
    var values = [
        data.username,
        data.email,
        moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
        id
    ];
    debug('values', values);
    return pg()
        .then(function(client){
            return Q.ninvoke(client, "query", {
                    text: "UPDATE users SET (username, email, updated) = ($1, $2, $3) WHERE id = $4",
                    values: values
                });
        })
        .then(function(result){
            debug('users.update result', result);
            if (result && result.rowCount) {
                return result;
            } else {
                return  null;
            }
        });
};

users.find = function(){
    return pg()
        .then(function(client){
            return Q.ninvoke(client, "query", "SELECT * FROM users");
        })
        .then(function(result){
            if (result  && result.rows) {
                return result.rows;
            } else {
                return [];
            }
        });
};

users.findOne = function(param, value){
    var values = [
        value
    ];
    return pg()
        .then(function(client){
            return Q.ninvoke(client, "query", {
                    text: "SELECT * FROM users WHERE " + param + " = $1",
                    values: values
                });
        })
        .then(function(result){
            debug('users.findOne result', result)
            if (result  && result.rows && result.rows.length === 1) {
                return result.rows[0];
            } else {
                return null;
            }
        });
};

users.delete = function(id){
    var values = [
        id
    ];
    return pg()
        .then(function(client){
            return Q.ninvoke(client, "query", {
                    text: "DELETE FROM users WHERE id = $1",
                    values: values
                });
        })
        .then(function(result){
            if (result && result.rowCount) {
                return result;
            } else {
                return null;
            }
        });
};

users.flush = function(){
    return pg()
        .then(function(client){
            return Q.ninvoke(client, "query", "TRUNCATE TABLE users;");
        });
};