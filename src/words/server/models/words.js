var _ = require('lodash');

var wordsModel = module.exports = {};

wordsModel.words = [];
wordsModel.id = 1;

wordsModel.getOne = function(id){
    return _.first(_.filter(wordsModel.words, function(user){
        return user.id === id;
    }));
};

wordsModel.getAll = function(){
    return wordsModel.words;
};

wordsModel.create = function(data){
    var user = {};

    user.id = wordsModel.id++;
    user.username = data.username || 'test_' + user.id;
    user.email = data.email || user.name + '@te.st';
    user.created = Date.now();

    wordsModel.words.push(user);

    return user;
};

wordsModel.update = function(id, data){
    var result = false;
    wordsModel.words.forEach(function(user, index){
        if (user.id === id) {
            wordsModel.words[index] = result = _.assign(user, data, {updated: Date.now()});
        }
    });
    return result;
};

wordsModel.delete = function(id){
    var result = false;
    wordsModel.words.forEach(function(user, index){
        if (user.id === id) {
            wordsModel.words.splice(index, 1);
            result = true;
        }
    });
    return result;
};

wordsModel.flush = function(){
    wordsModel.words = [];
    wordsModel.id = 1;
};