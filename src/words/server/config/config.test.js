var config = module.exports = {};

config.pg = {};
config.pg.user = 'test_words_api_user';
config.pg.password = 'password';
config.pg.database = 'test_words';
config.pg.host = 'localhost';
config.pg.port = 5432;

config.pg.url = "postgres://"
    + config.pg.user
    + ":"
    + config.pg.password
    + "@"
    + config.pg.host
    + ":"
    + config.pg.port
    + "/"
    + config.pg.database;