var debug = require('debug');

var appId = 'words';

module.exports = function(name){
    return debug(appId + ':' + name + ':');
};