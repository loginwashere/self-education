var pg = require('pg').native
  , Q = require('q')
  , config = require('../config')(process.env.NODE_ENV);

var client;

module.exports = function(){
    var deferred = Q.defer();

    if (client) {
        deferred.resolve(client);
    } else {
        client = new pg.Client(config.pg.url);
        client.connect(function(err){
            if (err) {
                deferred.reject(err);
            }
            deferred.resolve(client);
        });
    }

    return deferred.promise;
};