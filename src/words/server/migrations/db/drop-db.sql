
DROP DATABASE IF EXISTS words ;
DROP ROLE IF EXISTS words_api_user ;

DROP DATABASE IF EXISTS test_words ;
DROP ROLE IF EXISTS test_words_api_user ;