CREATE USER words_api_user password 'password' ;
CREATE DATABASE words OWNER words_api_user ;

CREATE USER test_words_api_user password 'password' ;
CREATE DATABASE test_words OWNER test_words_api_user ;