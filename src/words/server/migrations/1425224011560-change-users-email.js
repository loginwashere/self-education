var pg = require('./../lib/pg');

exports.up = function(next){
    return pg()
        .then(function(client){
            client.query(
                "ALTER TABLE users ALTER COLUMN email TYPE varchar(255) ;",
                function(err, result){
                    if (err) {
                        next(err);
                    } else {
                        next();
                    }
                }
            );
        });
};

exports.down = function(next){

};
