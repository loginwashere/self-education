'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    browserify = require('browserify'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    refresh = require('gulp-livereload'),
    jade = require('gulp-jade'),
    nodemon = require('gulp-nodemon'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    path = require('path');

gulp.task('words:serve', function () {
    nodemon({
        script: 'index.js',
        env: {
            TASK: 'words'
        },
        ext: 'json js',
        ignore: ['src/words/public/*', 'src/words/client/*']
    })
        .on('change', ['words:lint'])
        .on('restart', function () {
            console.log('Restarted webserver')
        });
});

gulp.task('words:prod', ['words:views', 'words:styles', 'words:lint', 'words:browserify'], function() {});

// Dev task
gulp.task('words:dev', ['words:prod', 'words:watch'], function() {});

// JSLint task
gulp.task('words:lint', function() {
    gulp.src('src/words/client/scripts/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Styles task
gulp.task('words:styles', function() {
    gulp.src('./src/words/client/styles/*.less')
        // The onerror handler prevents Gulp from crashing when you make a mistake in your SASS
        .pipe(less({
            paths: [
                path.join(__dirname + '/bower_components/')
            ]
        }))
        // Optionally add autoprefixer
        .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 8'))
        // These last two should look familiar now :)
        .pipe(gulp.dest('./src/words/public/css/'));
});

// Browserify task
gulp.task('words:browserify', function() {
    var bundleStream = browserify({
        entries: [
            './src/words/client/scripts/main.js'
        ],
        debug: process.env.NODE_ENV !== 'production'
    })
        .bundle()
        .pipe(source('scripts.js'))
        .pipe(buffer());

    if (process.env.NODE_ENV === 'production') {
        bundleStream
            .pipe(uglify({
                mangle: false
            }));
    }

    return bundleStream
        .pipe(gulp.dest('./src/words/public/js'));
});

// Views task
gulp.task('words:views', function() {
    // Get our index.html
    gulp.src('src/words/server/views/main.jade')
        .pipe(rename("index.html"))
        .pipe(jade({
            client: false,
            pretty: true
        }))
        // And put it in the public folder
        .pipe(gulp.dest('src/words/public/'));

    // Any other view files from client/views
    gulp.src('src/words/client/views/**/*')
        .pipe(jade({
            client: false,
            pretty: true
        }))
        // Will be put in the public/views folder
        .pipe(gulp.dest('src/words/public/views/'));
});

gulp.task('words:watch', ['words:serve', 'words:lint'], function() {
    // Start live reload server
    refresh.listen();

    // Watch our scripts, and when they change run lint and browserify
    gulp.watch([
        'src/words/client/scripts/*.js',
        'src/words/client/scripts/**/*.js'
    ],[
        'words:lint',
        'words:browserify'
    ]);

    // Watch our sass files
    gulp.watch(['src/words/client/styles/**/*.less'], [
        'words:styles'
    ]);

    // Watch view files
    gulp.watch([
        'src/words/client/views/**/*',
        'src/words/server/views/main.jade'
    ], [
        'words:views'
    ]);

    gulp.watch('./src/words/public/**').on('change', refresh.changed);

});

gulp.task('default', ['words:dev']);