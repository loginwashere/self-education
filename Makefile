REPORTER = spec

test:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require ./src/rest-api/bootstrap.js \
		--reporter $(REPORTER) \
		--ui tdd \
		test/ \
		test/words/routes/

test-cov:
	@NODE_ENV=test ./node_modules/.bin/istanbul cover ./node_modules/.bin/_mocha \
		--require ./src/rest-api/bootstrap.js \
		-- \
		-u exports \
		test/ \
		test/words/routes/

test-cov-view:
	cd ./coverage/lcov-report && python -m SimpleHTTPServer 8080

test-w:
	@NODE_ENV=test ./node_modules/.bin/mocha \
		--require ./src/rest-api/bootstrap.js \
		--reporter $(REPORTER) \
		--growl \
		--ui tdd \
		--watch

rest-api-init-db:
	sudo -u postgres psql --file=/var/www/self-education/src/rest-api/migrations/db/init-db.sql

rest-api-drop-db:
	sudo -u postgres psql --file=/var/www/self-education/src/rest-api/migrations/db/drop-db.sql

rest-api-migrate-up:
	./node_modules/.bin/migrate \
	-c ./src/rest-api \
	up

rest-api-migrate-down:
	./node_modules/.bin/migrate \
	-c ./src/rest-api \
	down

words-init-db:
	sudo -u postgres psql --file=/var/www/self-education/src/words/server/migrations/db/init-db.sql

words-drop-db:
	sudo -u postgres psql --file=/var/www/self-education/src/words/server/migrations/db/drop-db.sql

words-migrate-up:
	./node_modules/.bin/migrate \
	-c ./src/words/server \
	up

words-migrate-down:
	./node_modules/.bin/migrate \
	-c ./src/words/server \
	down

.PHONY: test test-w
