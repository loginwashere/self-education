#!/bin/bash

# colorize
sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' /home/vagrant/.bashrc

# locale
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

start=`date +%s`

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update

sudo apt-get -y install software-properties-common python-software-properties

sudo apt-add-repository ppa:chris-lea/node.js -y

sudo apt-get update

sudo apt-get -q -y  -o Dpkg::Options::=--force-confnew  install \
    nodejs \
    curl \
    htop \
    git \
    make \
    vim \
    g++ \
    build-essential \
    mc \
    postgresql \
    postgresql-contrib \
    libpq-dev

# Check versions
echo `node -v`
echo `npm -v`

# Insatll package for compiling native extensions
npm install -g node-gyp
npm install -g bower

cd /var/www/self-education

# Remove old npm dependencies
npm prune
# Install new npm dependencies
npm install
bower install --allow-root

# run rest-api specific setup script
./src/rest-api/bin/install.sh

# run words specific setup script
./src/words/server/bin/install.sh

echo "Ready to go"

end=`date +%s`

provisionTime=$((end - start))

echo "Provision took: '$provisionTime' seconds"
